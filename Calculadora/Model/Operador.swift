//
//  Operador.swift
//  Calculadora
//
//  Created by Aldair Cosetito on 5/21/20.
//

import Foundation

enum Operador: String {
    
    case suma = "+"
    case resta = "-"
    case multiplicacion = "x"
    case division = "/"
    case raizCuadrada = "√"
    
    func esPosibleOperar(numeroA:Double, numeroB:Double) -> Bool {
        switch self {
        case .division:
            return (numeroB == 0) ? false : true
        case .raizCuadrada:
            return (numeroA <= 0) ? false : true
        default:
            return true
        }
    }
    
    func operar(numeroA:Double, numeroB:Double) -> Double {
        switch self {
        case .suma:
            return numeroA + numeroB
        case .resta:
            return numeroA - numeroB
        case .multiplicacion:
            return numeroA * numeroB
        case .division:
            return (esPosibleOperar(numeroA: numeroA, numeroB: numeroB)) ? numeroA / numeroB : -99
        case .raizCuadrada:
            return (esPosibleOperar(numeroA: numeroA, numeroB: numeroB)) ? sqrt(numeroA) : -99
        }
    }
    
    func obtenerUltimoNumeroPresionado(Resultado resultado: Double, PrimerNumero primerNumero: Double) -> Double {
        switch self {
        case .suma:
            return resultado - primerNumero
        case .resta:
            return primerNumero - resultado
        case .multiplicacion:
            return resultado / primerNumero
        case .division:
            return primerNumero / resultado 
        case .raizCuadrada:
            return pow(resultado, 2.0)
        }
        
    }
    
}
