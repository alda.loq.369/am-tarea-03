//
//  CalculadoraViewController.swift
//  Calculadora
//

import UIKit

class CalculadoraViewController: UIViewController {

    // MARK: - Propiedades
    @IBOutlet weak var resultadoLabel: UILabel!
    @IBOutlet weak var igualButton: UIButton!
    var operation: Operador?
    var numeroA: Double = 0
    var valorConstante: Double?
    var ultimoResultado: Double?
    var presionadasIgualSeguidas:Int = 0
    
    // MARK: - Ciclo de vida
    override func viewDidLoad() {
        super.viewDidLoad()
        resultadoLabel.text = "0"
       
    }
    
    @IBAction func numberButtonTapped(_ sender: UIButton) {
        guard let buttonNumber = sender.titleLabel?.text else {
            return
        }
        
        guard let numeroString = resultadoLabel.text
            , numeroString != "0" else {
                resultadoLabel.text = buttonNumber
                return
        }
        resultadoLabel.text = numeroString + buttonNumber
    }

    @IBAction func operationButtonTapped(_ sender: UIButton) {
        guard let numeroString = resultadoLabel.text
            , let simboloOperador = sender.titleLabel?.text else {
            return
        }
        
        let operatorPressed = Operador(rawValue: simboloOperador)
        
        if operation != nil && presionadasIgualSeguidas == 0 && (operation?.rawValue != operatorPressed?.rawValue) {
            
            let result = calcularOperacion(Operacion: operation!, Numero1: numeroA, Numero2: Double(numeroString)!, Constante: valorConstante)
            
            mostrarValorEnResultadoLabel(Label: resultadoLabel, Resultado: result, Operacion: operation!)
            operation = operatorPressed
            numeroA = Double(numeroString)!
            return
        }
        
        numeroA = Double(numeroString)!
        operation = Operador(rawValue: simboloOperador)
        
        guard operation != Operador(rawValue: "√") else {
            presionadasIgualSeguidas = 0
            let result = calcularOperacion(Operacion: operation!, Numero1: numeroA, Numero2: 0.0, Constante: valorConstante)
            mostrarValorEnResultadoLabel(Label: resultadoLabel, Resultado: result, Operacion: operation!)
            return
        }
        
        resultadoLabel.text = "0"
        presionadasIgualSeguidas = 0
    }
    
    @IBAction func igualButtonTapped(_ sender: Any) {
        guard let numeroString = resultadoLabel.text
            , let miOperacion = operation else {
            return
        }
        let numeroB = Double(numeroString)!
        let result = calcularOperacion(Operacion: miOperacion, Numero1: numeroA, Numero2: numeroB, Constante: valorConstante)
        
        mostrarValorEnResultadoLabel(Label: resultadoLabel, Resultado: result, Operacion: miOperacion)
        
        valorConstante = (presionadasIgualSeguidas == 0) ? miOperacion.obtenerUltimoNumeroPresionado(Resultado: Double(result), PrimerNumero: Double(numeroA)): valorConstante
        
        presionadasIgualSeguidas += 1
    }
    
    //MARK: -Boton AC, limpia todos los valores de variables
    @IBAction func acButtonTapped(_ sender: UIButton) {
        operation = Operador(rawValue: "")
        numeroA = 0
        resultadoLabel.text = "0"
        presionadasIgualSeguidas = 0
    }
    
    //MARK: -Boton punto (.)
    @IBAction func puntoButtonTapped(_ sender: UIButton) {
        guard let buttonPunto = sender.titleLabel?.text,
            !(resultadoLabel.text!.contains(".")) else {
            return
        }
        resultadoLabel.text = resultadoLabel.text! + buttonPunto
    }
    
    //MARK: -Funciones locales
    func calcularOperacion (Operacion operacion: Operador, Numero1 numero1: Double, Numero2 numero2:Double, Constante constante:Double?) -> Double{
        
        guard operacion != Operador(rawValue: "-"),
         operacion != Operador(rawValue: "/") else{
            
            guard presionadasIgualSeguidas == 0 else {
                let result = operacion.operar(numeroA: numero2, numeroB: valorConstante!)
                return result
            }
            
            let result = operacion.operar(numeroA: numero1, numeroB: numero2)
            
            return result
        }
        
        guard operacion != Operador(rawValue: "√") else {
            let result = operacion.operar(
                numeroA: (presionadasIgualSeguidas == 0) ? numero1 : numero2,
                numeroB: numero2)
            
            presionadasIgualSeguidas += 1
            
            return result
        }
        
        let result = operacion.operar(
            numeroA: (presionadasIgualSeguidas >= 1) ? valorConstante!: numero1,
            numeroB: numero2)
        
        return result
    }
    
    func mostrarValorEnResultadoLabel(Label label:UILabel, Resultado resultado:Double, Operacion operacion:Operador) -> Void{
        
        guard (operacion != Operador(rawValue: "√") ||
            operacion != Operador(rawValue: "/"))  && (resultado != -99) else{
                label.text = "0"
                mostrarAlerta(title: "Error", message: "No es posible realizar la operacion, con los valores establecidos")
                return
        }
        
        label.text = String(format: (resultado - floor(resultado) > 0.000001) ? "%.2f" : "%.0f", resultado)
    }
    
    func mostrarAlerta(title: String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
